﻿

using IdentityModel.Client;

var client = new HttpClient();

await Task.Delay(5000);

var disco = await client.GetDiscoveryDocumentAsync("http://localhost:5001");
if (disco.IsError)
{
    Console.WriteLine(disco.Error);
    return;
}

// request token
var tokenResponse = await client.RequestClientCredentialsTokenAsync(
    new ClientCredentialsTokenRequest
{
        Address = disco.TokenEndpoint,
        ClientId = "console-client",
        ClientSecret = "Lem0nC0deSecret",
        Scope = "chat-api"
    });

if (tokenResponse.IsError)
{
    Console.WriteLine(tokenResponse.Error);
    return;
}

Console.WriteLine(tokenResponse.AccessToken);

Console.ReadLine();