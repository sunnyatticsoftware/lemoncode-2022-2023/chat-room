﻿using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("[controller]")]
public class HealthController
    : ControllerBase
{
    private readonly IWebHostEnvironment _environment;
    private readonly IConfiguration _configuration;

    public HealthController(
        IWebHostEnvironment environment,
        IConfiguration configuration)
    {
        _environment = environment;
        _configuration = configuration;
    }

    [HttpGet]
    public IActionResult Health()
    {
        var environmentName = _environment.EnvironmentName;
        var lemoncodeSecretWord = _configuration.GetValue<string>("LemoncodeSecretWord");
        var assemblyName = typeof(Startup).Assembly.GetName();
        var currentUtcDateTime = DateTime.UtcNow;
        var result = $"Assembly={assemblyName}, Environment={environmentName}, " +
                     $"CurrentUtcTime={currentUtcDateTime}, SecretWord={lemoncodeSecretWord}";
        return Ok(result);
    }
}
